import java.util.Scanner;

public class Roulette {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        RouletteWheel wheel = new RouletteWheel();
        int userMoney = 1000;

        while (userMoney > 0) {
            System.out.println("Your current balance: $" + userMoney);
            System.out.print("Do you want to make a bet? (yes/no): ");
            String choice = scanner.nextLine().toLowerCase();

            if (choice.equals("yes")) {
                int betAmount = getBetAmount(userMoney, scanner);
                int betNumber = getBetNumber(scanner);

                wheel.spin();
                int spinResult = wheel.getValue();

                int winnings = calculateWinnings(betAmount, betNumber, spinResult);
                userMoney += winnings;

                if (winnings > 0) {
                    System.out.println("Congratulations! You won $" + winnings);
                } else {
                    System.out.println("Sorry, you lost $" + (-winnings)); // "-" necessary or else it gives neg. number
                }
            } else if (choice.equals("no")) {
                break;
            } else {
                System.out.println("Invalid input. Please enter 'yes' or 'no'.");
            }
        }

        System.out.println("Game over. Your final balance: $" + userMoney);
        scanner.close();
    }

    private static int getBetAmount(int userMoney, Scanner scanner) {
        int betAmount;
        while (true) {
            System.out.print("Enter your bet amount (1-" + userMoney + "): ");
            betAmount = scanner.nextInt();
            scanner.nextLine(); 

            if (betAmount >= 1 && betAmount <= userMoney) {
                return betAmount;
            } else {
                System.out.println("Invalid bet amount. Please enter a valid amount.");
            }
        }
    }

    private static int getBetNumber(Scanner scanner) {
        int betNumber;
        while (true) {
            System.out.print("Enter the number you want to bet on (0-36): ");
            betNumber = scanner.nextInt();
            scanner.nextLine(); 

            if (betNumber >= 0 && betNumber <= 36) {
                return betNumber;
            } else {
                System.out.println("Invalid bet number. Please enter a number between 0 and 36.");
            }
        }
    }

    private static int calculateWinnings(int betAmount, int betNumber, int spinResult) {
        if (betNumber == spinResult) {
            return betAmount * 35; 
        } else {
            return -betAmount; 
        }
    }
}

