import java.util.Random;

public class RouletteWheel {

    private Random random;
    private int lastSpin;
    
    public RouletteWheel() {
        random = new Random();
        lastSpin = 0;
    }

    public void spin() {
        lastSpin = random.nextInt(37);
    }

    public int getValue() {
        return lastSpin;
    }

}
